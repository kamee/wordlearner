package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"math/rand"

	"gitlab.com/kamee/wordlearner/remove"
)

func writeFile(appendLine1, appendLine2 string, write bool){
	if _, err := os.Stat("correct.txt"); os.IsNotExist(err) {
		_, err = os.Create("correct.txt")
	        if err != nil {
			fmt.Println(err)
		}
	}

	if write {
		f, err := os.OpenFile("correct.txt", os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			fmt.Println(err)
			f.Close()
			return
		}

		//    _, err = fmt.Fprintln(f, appendLine1, "\n", appendLine2)
		_, err = fmt.Fprintln(f, appendLine1)
		_, err = fmt.Fprintln(f, appendLine2 + "\n")
		if err != nil {
			fmt.Println(err)
			f.Close()
			return
		}
		err = f.Close()
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}

func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	 return lines, scanner.Err()
}

func checkAnswers(filename string, write bool){
	var answer string
	lines, err := readLines(filename)
	length := len(lines)
        question := rand.Intn(length)
	if err != nil {
		log.Fatal(err)
	}

	if lines[question] != "" {
		fmt.Println("input the translation of the word: ", lines[question])
		fmt.Print("your answer is: ")
		fmt.Scanln(&answer)

		if question % 3 == 0 {
			if strings.Contains(strings.ToLower(lines[question+1]), strings.ToLower(answer)) && answer != ""{
				remove.RemoveLines(filename, question, 3)
				writeFile(lines[question], lines[question+1], write)
				fmt.Println("your answer is correct: ", lines[question+1])
				} else {
					fmt.Println("sorry, incorrect, the correct answer is: ", lines[question+1])
				}
		}

		if question % 3 == 1 {
			if strings.Contains(strings.ToLower(lines[question-1]), strings.ToLower(answer)) && answer != ""{
				remove.RemoveLines(filename, question, 3)
				writeFile(lines[question], lines[question-1], write)
				fmt.Println("your answer is correct: ", lines[question-1])
			} else {
				fmt.Println("sorry, inccorect, the correct answer is: ", lines[question-1])
			}
		}
	}
}

func main() {
	var i int
	var answer string
	for i <= 10 {
		checkAnswers("de.babylon", true)
		i = i + 1
	}

	fmt.Print("do you want to repeat your previous correct answers?(please type y or n): ")
        fmt.Scanln(&answer)
	if answer == "y"{
		for i <= 15 {
			checkAnswers("correct.txt", false)
			i = i + 1
		}
		fmt.Println("thats enough for today, bye")
	}
	if answer == "n"{
		fmt.Println("bye, bye, have a good day!")
	}
}
